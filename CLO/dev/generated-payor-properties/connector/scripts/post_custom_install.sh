#!/bin/sh

####################################################################################################
# This script will be invoked and executed on Connector host by he-installer after installing Connector
# features and before invoking health check step.

#
# Make sure all the dependent files are present in this directory.
#
# The he-installer passes the following arguments in the same order to this script to use if needed
# $1 => HealthRulesConnectorServer Location
# $2 => Connector distribution location name
# $3 => Username being used to connect to HealthRulesConnectorServer
# $4 => Hostname

##################################################################################################

# Uncomment the following line to invoke providersearch script to copy and configure the buleprints and create indices in Elasticsearch
mkdir -p /home/clover_cprime/cprime/healthrules-connector-server-4.0.4/generic-extract-scheduler/staging
mkdir -p /home/clover_cprime/cprime/output-extracts
./clover_claim_header_configure.sh $1 $4
./clover_claim_lines_configure.sh $1 $4
./clover_condition_code_configure.sh $1 $4
./clover_diagnosis_code_configure.sh $1 $4
./clover_occurrence_code_configure.sh $1 $4
./clover_procedure_code_configure.sh $1 $4
./clover_value_code_configure.sh $1 $4
./clover_message_codes_lines_configure.sh $1 $4
./clover_message_codes_header_configure.sh $1 $4
./clover_claim_header_configure_pended.sh $1 $4
./clover_claim_lines_configure_pended.sh $1 $4
./clover_diagnosis_code_configure_pended.sh $1 $4
./clover_occurrence_code_configure_pended.sh $1 $4
./clover_procedure_code_configure_pended.sh $1 $4
./clover_value_code_configure_pended.sh $1 $4
./clover_condition_code_configure_pended.sh $1 $4
./clover_message_codes_header_configure_pended.sh $1 $4
./clover_message_codes_lines_configure_pended.sh $1 $4
